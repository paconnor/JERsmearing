#include <cstdlib>
#include <cmath>
#include <iostream>
#include <vector>
#include <random>
#include <algorithm>

#include <TApplication.h>
#include <TCanvas.h>
#include <TString.h>
#include <TH1.h>

using namespace std;

//static const vector<double> pt_edges {43,49,56,64,74,84};
static const vector<double> pt_edges {43,49,56,64,74,84,97,114,133,153,174,196,220,245,272,300,330,362,395,430,468,507,548,592,638,686,737,790,846,905,967,1032,1101,1172,1248,1327,1410,1497,1588,1684,1784,1890,2000,3000};
static const int nptbins = pt_edges.size()-1;
static const double ptmin = pt_edges.front(), ptmax = pt_edges.back();

void Draw (TH1 * pt_original, TH1 * pt_smeared)
{
    TCanvas * c = new TCanvas("smearing", "smearing", 800, 600);

    pt_smeared->Divide(pt_original);

    // title y axis range
    pt_smeared->SetTitle(";p_{T}   [GeV];#sigma_{smeared}/#sigma_{original}");
    pt_smeared->SetMaximum(1.05);
    pt_smeared->SetMinimum(0.98);

    pt_smeared->Draw();
    
    // axis style
    pt_smeared->GetXaxis()->SetMoreLogLabels();
    pt_smeared->GetXaxis()->SetNoExponent();
    gPad->SetLogx();

    c->Print("example.pdf", "Title:smearing");
}


pair<double, double> generateJet ()
{
    // generate a number between 0 and 1
    thread_local mt19937 random(18);
    double r = random(); r /= mt19937::max();

    // exponent for steeply falling distribution
    static const int p = 5;
    
    double pt = ptmin + r*(ptmax-ptmin);
    double weight  = 1./pow(pt,p);

    return {pt, weight};
}

double getResolution (double pt)
{
    return 0.5/(1.+sqrt(pt))+0.01;
}

double getSF (double pt)
{
    return 1.1;
}

double smearPt (double pt)
{
    // using stochastic smearing
    thread_local mt19937 random(42);

    // get resolution and SF
    double jet_resolution = getResolution(pt);
    double jer_sf = getSF(pt);

    // define gaussian
    double sigma = jet_resolution * sqrt(jer_sf * jer_sf - 1);
    normal_distribution<> gaus(0, sigma);

    // smear
    double smearFactor = 1. + gaus(random);
    pt *= smearFactor;

    return pt;
}

inline TH1 * generateHistogram (TString name)
{
    return new TH1D(name, name, nptbins, pt_edges.data());
}

int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();

    static const int npoints = argc > 1 ? atoi(argv[1]) : 1000000000;

    TApplication* application = new TApplication("smearing", &argc, argv);

    TH1 * h_raw = generateHistogram("raw"),
        * h_smeared = generateHistogram("smeared");

    // generate events
    for (int n1 = 0; n1 < npoints; ++n1) {
        double pt, weight;
        tie(pt, weight) = generateJet();

        h_raw->Fill(pt, weight);

        pt = smearPt(pt);

        h_smeared->Fill(pt, weight);
    }

    Draw(h_raw, h_smeared);

    /// run
    application->Run();

    return EXIT_SUCCESS;
}
