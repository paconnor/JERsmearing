#ROOT includes + libraries
ROOTCFLAGS = $(shell root-config --cflags)
ROOTLIBS   = $(shell root-config --libs)


#Debugging info + allWarnings + level-3 optimalisation
CFLAGS=-g -Wall -O3

all: example

# Rule to compile examples
example: example.cc
	g++ $(CFLAGS) $(ROOTCFLAGS)   $^  $(ROOTLIBS)  -o $@ 

run:
	./example

clean:
	rm -f *.o example example



